package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

import static java.lang.Integer.min;

public final class AttackSubsystemImpl implements AttackSubsystem {
    private String name;

    private PositiveInteger baseDamage;

    private PositiveInteger optimalSize;

    private PositiveInteger optimalSpeed;

    private PositiveInteger capacitorUsage;

    private PositiveInteger pgRequirement;

    public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
                                                PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                                                PositiveInteger baseDamage) throws IllegalArgumentException {

        if (isEmpty(name)) {
            throw new IllegalArgumentException("Name should be not null and not empty");
        }
        return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize, baseDamage);
    }


    public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments,
                               PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                               PositiveInteger baseDamage) {
        this.name = name;
        this.pgRequirement = powergridRequirments;
        this.capacitorUsage = capacitorConsumption;
        this.optimalSize = optimalSize;
        this.optimalSpeed = optimalSpeed;
        this.baseDamage = baseDamage;

    }

    @Override
    public PositiveInteger getPowerGridConsumption() {
        // TODO: Ваш код здесь :)
        return pgRequirement;
    }

    @Override
    public PositiveInteger getCapacitorConsumption() {
        // TODO: Ваш код здесь :)
        return capacitorUsage;
    }

    @Override
    public PositiveInteger attack(Attackable target) {
        PositiveInteger sizet = target.getSize();
        PositiveInteger sizeo = optimalSize;
        Integer sizeo1 = sizeo.value();
        Integer sizet1 = sizet.value();

        PositiveInteger spt = target.getCurrentSpeed();
        PositiveInteger spo = optimalSpeed;
        Integer spt1 = spt.value();
        Integer spo1 = spo.value();

        Integer baseDamage1 = baseDamage.value();

        Integer[] obj2 = new Integer[]{sizeo1, sizet1, spt1, spo1, baseDamage1};
        double[] result = new double[obj2.length];

        double sRM1;
        double sRM2;
        double damage1 = 0.0;

        for (int i = 0; i < obj2.length; i++) {
            result[i] = obj2[i].doubleValue();
        }
        double sizeo1d =result[0];
        double sizet1d = result[1];
        double spt1d =result[2];
        double spo1d = result[3];
        double baseDamage1d = result[4];

        if (sizet1d >= sizeo1d) {
            sRM1 = 1;
        } else {
            sRM1 = sizet1d / sizeo1d;
        }
        if (spt1d <= spo1d) {
            sRM2 = 1;
        } else {
            sRM2 = Math.ceil(spo1d/ spt1d)/ 2.0;
        }
        double damage3 = Math.min(sRM1, sRM2);
        damage1 = Math.ceil(baseDamage1d * damage3);

        int damage0 = (int) damage1;
        Integer damage2 = Integer.valueOf(damage0);
        return com.binary_studio.fleet_commander.core.common.PositiveInteger.of(damage2);

    }


    @Override
    public String getName() {
        return name;
    }

    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        }
        for (char c : str.toCharArray()) {
            if (!Character.isSpaceChar(c)) {
                return false;
            }
        }
        return true;
    }

}
