package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;


public final class DockedShip implements ModularVessel {

    private String name;

    private PositiveInteger shieldHP;

    private PositiveInteger hullHP;

    private PositiveInteger capacitor;

    private PositiveInteger capacitorRegeneration;

    private PositiveInteger pg;

    private PositiveInteger speed;

    private PositiveInteger size;

    public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
                                       PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
                                       PositiveInteger speed, PositiveInteger size) {
        // TODO: Ваш код здесь :)
        return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed, size);
    }

    public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
                      PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
                      PositiveInteger speed, PositiveInteger size) {
        this.name = name;
        this.shieldHP = shieldHP;
        this.hullHP = hullHP;
        this.pg = powergridOutput;
        this.capacitor = capacitorAmount;
        this.capacitorRegeneration = capacitorRechargeRate;
        this.speed = speed;
        this.size = size;
    }

    @Override
    public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
        // TODO: Ваш код здесь :)
        Integer missingPowergrid =pg.value()-subsystem.getPowerGridConsumption().value();
        if(subsystem.getPowerGridConsumption().value()<=pg.value()){
            throw new InsufficientPowergridException(missingPowergrid);
        }
    }

    @Override
    public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
        // TODO: Ваш код здесь :)
        
        Integer missingPowergrid =pg.value()-subsystem.getPowerGridConsumption().value();
        if(subsystem.getPowerGridConsumption().value()<=pg.value()){
            throw new InsufficientPowergridException(missingPowergrid);
        }
        
    }

    public CombatReadyShip undock() throws NotAllSubsystemsFitted {
        // TODO: Ваш код здесь :)
        return null;
    }

}
