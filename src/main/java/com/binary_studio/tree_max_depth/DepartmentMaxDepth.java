package com.binary_studio.tree_max_depth;

public final class DepartmentMaxDepth {

    private DepartmentMaxDepth() {
    }

    public static Integer calculateMaxDepth(Department rootDepartment) {
        int result;
        if (rootDepartment == null) {
            result = 0;
        } else if (rootDepartment.subDepartments.isEmpty()) {
            result = 1;
        } else {
            int max = 0;
            for (Department subDepartment : rootDepartment.subDepartments) {
                int depth = calculateMaxDepth(subDepartment);
                max = Integer.max(max, depth);
            }
            result = max + 1;
        }
        return result;
    }

}
